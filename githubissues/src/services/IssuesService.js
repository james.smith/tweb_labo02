
export function IssuesData(){
  return [{
    id: 1, name: 'Google Drive API "batching" feature to increase write throughput', isOpen: true, owner: 'Gallouch', date: new Date('2018-05-21T14:24:00')
  },
  {
    id: 2, name: 'sync from encrypt remote to encrypt remote', isOpen: true, owner: 'Jeremie-Chatillon', date: new Date('2018-05-22T19:24:00')
  },{
    id: 3, name: 'Single backslash character in path stops the sync', isOpen: true, owner: 'ouzgaga', date: new Date('2018-08-01T11:06:00')
  },{
    id: 4, name: 'cannot list openstack swift bucket content', isOpen: true, owner: 'nooka10', date: new Date('2018-11-02T22:13:00')
  } ,{
    id: 5, name: 'this isnt the test you think it is', isOpen: false, owner: 'james.smith', date: new Date('2018-11-04T12:48:00')
  } 
  ];
}

export function IssuesDataDetails(id){
  const issues =  [{
    id: 1, name: 'Google Drive API "batching" feature to increase write throughput', isOpen: true, owner: 'Gallouch', date: new Date('2018-05-21T14:24:00'), description: "this is a small description of the issues. I love PRR <3"
  },
  {
    id: 2, name: 'sync from encrypt remote to encrypt remote', isOpen: true, owner: 'Jeremie-Chatillon', date: new Date('2018-05-22T19:24:00'), description: "this is a small description of the issues. Nice one!"
  },{
    id: 3, name: 'Single backslash character in path stops the sync', isOpen: true, owner: 'ouzgaga', date: new Date('2018-08-01T11:06:00'), description: "this is a small description of the issues. Not really"
  },{
    id: 4, name: 'cannot list openstack swift bucket content', isOpen: true, owner: 'nooka10', date: new Date('2018-11-02T22:13:00'), description: "this is a small description of the issues. This is a fake info"
  } ,{
    id: 5, name: 'this isnt the test you think it is', isOpen: false, owner: 'james.smith', date: new Date('2018-11-04T12:48:00'), description: "this is a small description of the issues. Just for fun"
  } 
  ];
  let issue = findObjectByKey(issues,'id',id);
  return issue;
}

function findObjectByKey(array, key, value) {
  for (var i = 0; i < array.length; i++) {
      if (String(array[i][key]) === value) {
          return array[i];
      }
  }
  return null;
}
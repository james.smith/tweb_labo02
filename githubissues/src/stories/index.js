import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';

import { Button, Welcome } from '@storybook/react/demo';

import IssuesTable from '../components/IssuesTable'
import IssueRowTable from '../components/IssueRowTable';
import DetailIssue from '../components/DetailIssue';

  const issues = 
    [
      {
        "cursor": "Y3Vyc29yOnYyOpK5MjAxOC0xMS0yMFQxMjozMzoxOSswMTowMM4WzliO",
        "node": {
          "title": "Cusotmizing TextField",
          "author": {
            "login": "Serenny"
          },
          "number": 13652,
          "createdAt": "2018-11-20T11:33:19Z",
          "id": "MDU6SXNzdWUzODI2MjE4Mzg=",
          "state": "OPEN"
        }
      },
      {
        "cursor": "Y3Vyc29yOnYyOpK5MjAxOC0xMS0yMFQxMDowNjo1NiswMTowMM4WzXUW",
        "node": {
          "title": "[Slider] Knob transform causes horizontal scrollbars",
          "author": {
            "login": "rolandjitsu"
          },
          "number": 13649,
          "createdAt": "2018-11-20T09:06:56Z",
          "id": "MDU6SXNzdWUzODI1NjM2MDY=",
          "state": "OPEN"
        }
      },
      {
        "cursor": "Y3Vyc29yOnYyOpK5MjAxOC0xMS0yMFQwOTozNTo1OCswMTowMM4WzUs-",
        "node": {
          "title": "[Dialog] onBackdropClick event fires when draggable item released on it",
          "author": {
            "login": "apo-mcc"
          },
          "number": 13648,
          "createdAt": "2018-11-20T08:35:58Z",
          "id": "MDU6SXNzdWUzODI1NTI4OTQ=",
          "state": "CLOSED"
        }
      }
    ];

const issueDetailValues = {
  "title": "[Slider] Knob transform causes horizontal scrollbars",
  "author": {
    "login": "rolandjitsu"
  },
  "number": 13649,
  "state": "OPEN",
  "body": "<!--- Provide a general summary of the issue in the Title above -->\r\nIf the container of a `<Slider>` has no width set, or is set to `100%`, the knob transform seems to create horizontal scroll.\r\n\r\n<!--\r\n    Thank you very much for contributing to Material-UI by creating an issue! ❤️\r\n    To avoid duplicate issues we ask you to check off the following list.\r\n-->\r\n\r\n<!-- Checked checkbox should look like this: [x] -->\r\n- [x] This is not a v0.x issue. <!-- (v0.x is no longer maintained) -->\r\n- [x] I have searched the [issues](https://github.com/mui-org/material-ui/issues) of this repository and believe that this is not a duplicate.\r\n\r\n## Expected Behavior\r\n<!---\r\n    Describe what should happen.\r\n-->\r\nThe slider should not create any scrolling inside the container.\r\n\r\n## Current Behavior\r\n<!---\r\n    Describe what happens instead of the expected behavior.\r\n-->\r\nThe slider creates scroll.\r\n\r\n## Steps to Reproduce\r\n<!---\r\n    Provide a link to a live example (you can use codesandbox.io) and an unambiguous set of steps to reproduce this bug.\r\n    Include code to reproduce, if relevant (which it most likely is).\r\n\r\n    This codesandbox.io template _may_ be a good starting point:\r\n    https://codesandbox.io/s/github/mui-org/material-ui/tree/master/examples/create-react-app\r\n\r\n    If you're using typescript a better starting point would be\r\n    https://codesandbox.io/s/github/mui-org/material-ui/tree/master/examples/create-react-app-with-typescript\r\n\r\n    If YOU DO NOT take time to provide a codesandbox.io reproduction, should the COMMUNITY take time to help you?\r\n\r\n-->\r\nLink: https://codesandbox.io/s/njo6wm6y4\r\n\r\n1. Move the slider left/right\r\n2. Notice the scrolling on X increases as the knob is moved to the right\r\n3. If `position: relative` is removed from the button (the knob), the scrolling is gone (but of course the knob is not positioned properly anymore)\r\n4.\r\n\r\n## Context\r\n<!---\r\n    What are you trying to accomplish? How has this issue affected you?\r\n    Providing context helps us come up with a solution that is most useful in the real world.\r\n-->\r\nI have a bunch of sliders inside a [react-swipeable-views](https://github.com/oliviertassinari/react-swipeable-views) container.\r\n\r\n## Your Environment\r\n<!---\r\n    Include as many relevant details about the environment with which you experienced the bug.\r\n    If you encounter issues with typescript please include version and tsconfig.\r\n-->\r\n\r\n| Tech         | Version |\r\n|--------------|---------|\r\n| Material-UI  | v3.4.0  |\r\n| React        |   v16.6.3      |\r\n| Browser      |    Chrome v70     |\r\n| TypeScript   |    v2.9.2     |\r\n| etc.         |         |\r\n"
};

storiesOf('IssuesTable', module)
  .add('show issues', () => (
  <IssuesTable issues={issues} repo="material-ui" owner="mui-org"/>
  ));

storiesOf('Issue Row', module)
  .add('Open issue', () => (
    <IssueRowTable issue={issues[0]} owner="mui-org" repo="material-ui"/>
  ))
  .add('Closed issue', ()=>(     
    <IssueRowTable issue={issues[2]} owner="mui-org" repo="material-ui"/>
  ));

storiesOf('Issue Details', module)
  .add('Details Issue', () =>(
    <DetailIssue issue={issueDetailValues}/>
  ));
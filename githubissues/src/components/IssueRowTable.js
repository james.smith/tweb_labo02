
import React from 'react';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Info from '@material-ui/icons/Info';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Moment from 'react-moment';

const styles = {
  openIssue: {
    color: "#4caf50"
  },
  closedIssue:{
    color: "#F44336",
  },
  title: {
    fontSize: 16,
    color: '#24292e',
    lineHeight: '2px',
    marginTop: '20px',
    marginBottom: '0px',
    fontWeight: 'bold',
  },
  link: {
    textDecoration: 'none'
  }
}


class IssueRowTable extends React.Component{

  handleClick = () => {
    this.props.changeRoot("/details/" + this.props.owner + "/" + this.props.repo + "/" + this.props.issue.node.number);
  }

  render(){
    const {issue} = this.props;
    const {classes} = this.props;
    return (
      <TableRow key={issue.id}>
        <TableCell>
          {issue.node.state === "OPEN" ? 
            <Info className={classes.openIssue}/> :
            <Info className={classes.closedIssue}/> 
          }
          </TableCell>
        <TableCell scope="row" >
          <h4 onClick={this.handleClick} className={classes.title}>{issue.node.title}</h4>
          <p>#{issue.node.number} open <Moment fromNow>{issue.node.createdAt}</Moment> by {issue.node.author.login}</p>
        </TableCell>
      </TableRow>
    );
  }
}

IssueRowTable.propTypes = {
  classes: PropTypes.object.isRequired,
  className: PropTypes.string,
};

export default withStyles(styles)(IssueRowTable);
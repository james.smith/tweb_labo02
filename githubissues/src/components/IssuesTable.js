import React, { Component } from 'react';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import Paper from '@material-ui/core/Paper';
import IssueRowTable from './IssueRowTable';

class IssuesTable extends Component {
  constructor(props){
    super(props);
    this.state = {}
  }


  render(){
    const {issues, owner, repo} = this.props;
    return(
      <Paper>
        <Table>
          <TableBody>
            {issues.map(issue => {
              return (
                <IssueRowTable issue={issue} changeRoot={this.props.changeRoot} owner={owner} repo={repo}/>
              );
            })}
          </TableBody>
        </Table>
      </Paper>
    );
  }
}

export default IssuesTable;
import React from 'react';
import ErrorNoMatch from './ErrorNoMatch';
import { Paper, Grid, Divider} from '@material-ui/core';
import Info from '@material-ui/icons/Info';
import Moment from 'react-moment';

/** STYLE **/
const openIssue = {
  paddingTop: '10px',
    color: "#4caf50"
  };
const closedIssue = {
  paddingTop: '10px',
    color: "#F44336"
  };

const title = {
    fontSize: 16,
    fontWeight: 400,
    color: '#24292e',
    marginTop: '15px',
    marginBottom: '0px',
};

const issueNumber = {
  fontSize: 16,
  color: '#a3aab1',
  fontWeight: 300,
  letterSpacing: '-1px',
  paddingRight: '30px'
};

const description = {
  fontSize: '0.8125rem',
  paddingLeft: '30px',
  paddingRight: '30px'
}

const details = {
  fontSize: '0.8125rem',  
  paddingRight: '30px'
}

const container = {
  width: '600px',
  textAlign: 'left'
}

const infoIcon = {
  textAlign: "center",
}

function Detail(props){
  const {issue} = props;
  return(
    <Paper style={container}>
      <Grid container>
      {/*Title*/}
        <Grid item sm={2}>
          <div style={infoIcon}>
            {issue.state === "OPEN" ? 
                <Info style={openIssue}/> 
              :
                <Info style={closedIssue}/> 
              }
          </div>
        </Grid>
        <Grid item sm={10}>
          <h1 style={title}>
            <span>  
              {issue.title + " "}
            </span>
            <span style={issueNumber}>
              #{issue.number}
            </span>
          </h1>
        </Grid>
      </Grid>
      <Grid container>
        {/*Content*/}
        <Grid item xs={2}></Grid>
        <Grid item xs={10}>
          <p style={details}>{issue.owner} open this issue <Moment fromNow>{issue.createdAt}</Moment></p>        
        </Grid>
      </Grid>
      <Divider/>  
      <Grid container>
        <Grid item xl={12}>
          <p style={description}>{issue.body}</p>
        </Grid>
      </Grid>
    </Paper>
  );
}

class DetailIssue extends React.Component{
  render(){
    const {issue} = this.props;
    return (
      <div>
        {issue ?
          <Detail issue={issue}/>
        :
          <ErrorNoMatch/>
        }
      </div>
    );
  }
}


export default DetailIssue;
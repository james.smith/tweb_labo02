import React from 'react';
import IssuesTable from './IssuesTable';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';
import CircularProgress from '@material-ui/core/CircularProgress';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

const query = gql`
query issuesRep($owner: String!, $repo: String!, $state: [IssueState!]){
  repository(owner: $owner, name: $repo){
    name,
    owner{
      login
    },
    issues(orderBy: {field: CREATED_AT, direction: DESC}, states: $state, first: 50){
      edges{
        cursor,
        node{
          title,
          author{
            login
          },
          number,
          createdAt,
          id,
          state
        }
      }
    }
  }
}
`

class Issues extends React.Component {
  constructor(props){
    super(props);
    this.state={
      stateIssue : "OPEN"
    }
  }

  handleChange = event => {
    this.setState({ stateIssue: event.target.value });
  };

  render(){
    const {owner, repo, changeRoot} = this.props;
    const state = this.state.stateIssue;
    return (
      <Query query={query} variables={{ owner, repo,  state}}>
      {({loading, error, data}) => {
        if(loading) return <CircularProgress/>;
        if(error) return <h1>Error: error in the request to the server</h1>;
        const {edges} = data.repository.issues;
        const {name, owner} = data.repository;
        return (
          <div>
            <div>
              <FormControl>
                <InputLabel htmlFor="status-issue">Status</InputLabel>
                <Select
                  value={this.state.stateIssue}
                  onChange={this.handleChange}
                  inputProps={{
                    name: 'status-issue',
                    id: 'status-issue',
                  }}
                >
                  <MenuItem value="OPEN">
                    OPEN
                  </MenuItem>
                  <MenuItem value="CLOSED">CLOSED</MenuItem>
                </Select>
              </FormControl>
            </div>
            <div>
              <IssuesTable issues={edges} changeRoot={changeRoot} repo={name} owner={owner.login}/>
            </div>
          </div>
        );
      }}
      </Query>
    );
  }
}

export default Issues;
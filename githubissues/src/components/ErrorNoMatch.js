import React from 'react';

function ErrorNoMatch(props){
  return (
    <h1>404 not found</h1>
  );
}

export default ErrorNoMatch;
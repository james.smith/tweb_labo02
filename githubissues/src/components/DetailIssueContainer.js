import React from 'react';
import DetailIssue from './DetailIssue';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';


const query = gql`
query issueRep($number: Int!, $owner: String!, $repo: String!){
  repository(owner: $owner, name: $repo){
    issue(number: $number){
     	title,
      author {
        login
      },
      number,
      state,
      body,
    }
  }
}
`

class DetailIssueContainer extends React.Component{
  render(){
    const {owner, repo} = this.props.match.params;
    const number = parseInt(this.props.match.params.id, 10);
    return (
      <Query
      query={query}
      variables={{ number,owner, repo }}
    >
      {({ loading, error, data }) => {
        if (loading) return <div>Loading...</div>;
        if (error) return <div>Error :(</div>;
        const {issue} = data.repository;
        return <DetailIssue issue={issue}/>;
      }}
      </Query>
    );
  }
}

export default DetailIssueContainer;
import React, { Component } from 'react';
import Issues from './components/Issues';
import { Route , Switch } from 'react-router-dom';
import ErrorNoMatch from './components/ErrorNoMatch';

import './App.css';
import DetailIssueContainer from './components/DetailIssueContainer';

import PropTypes from "prop-types";


class App extends Component {
  static contextTypes = {
    router: PropTypes.object

  }
  constructor(props, context){
    super(props, context);
    this.changeRoot = this.changeRoot.bind(this);
  }

  changeRoot(path){
    console.log("change root to " + path);
    this.context.router.history.push(path);
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <div>
            <Switch>
              <Route exact path="/" render={()=><Issues changeRoot={this.changeRoot} owner="mui-org" repo="material-ui" state={["OPEN","CLOSED"]}/>}/>
              <Route exact path="/details/:owner/:repo/:id" component={DetailIssueContainer} />
              <Route component={ErrorNoMatch} />
            </Switch>
          </div>
        </header>
      </div>
    );
  }
}

export default App;
